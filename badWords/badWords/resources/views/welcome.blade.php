<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }
            .special{

                width : 300px;
                height: 50px;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Arial', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>

    <div>
        <form action="index" role="form" method="post">
{!! csrf_field() !!}

            <div>
                <lable for="lang">Insert Your Language :  </lable>
            </div>
            <div>
                <select name="language">
                    <option value="en">English</option>
                    <option value="fr">Fr</option>
                    <option value="fa">Fa</option>
                    <option value="ar">Ar</option>
                </select>
            </div>
            <input class="special"  type="text" name="text" maxlength="150" placeholder="Insert Text" />
            <div>
                <input type="submit" value="Submit">
            </div>
            {{--<div>--}}
                {{--<div>--}}
                    {{--<lable for="result">Edited Text :  </lable>--}}
                {{--</div>--}}
{{--                {{  $newText }}--}}
            {{--</div>--}}
            {{--<input class="special" type="text" onselect="disabled" name="textResult"/>--}}

        </form>
    </div>
        <div class="container">
            <div class="content">
                {{--<div class="title">Laravel 5</div>--}}
            </div>
        </div>
    </body>
</html>
