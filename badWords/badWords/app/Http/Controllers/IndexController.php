<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{

    public function __construct(Request $request)
    {

    }
    public function postText(Request $request)
    {
        $text = $request->text ;
        $lang = $request->language;
        
        return $this->getText($text,$lang);
    }

    public function getText($text,$lang)
    {
        $arr_user = explode(' ', $text);
        $str_lang = $lang;
        $str1 =file_get_contents($str_lang,FILE_USE_INCLUDE_PATH);

        $arr_file=explode("\n",$str1);

        $arr_int = array_intersect($arr_user,$arr_file);

        foreach ($arr_user  as $key=>$value)
        {
            foreach ($arr_int as $key1=>$value1)
            {
                if ($value == $value1)
                {
                    $arr_user[$key]= '???';
                }
            }
        }
//        $arr_res = array_diff($arr_user,$arr_file);
        $newText =implode(" ", $arr_user);
//        dd($newText);
        return view('result')
            ->with(compact('newText','text'));
    }

}
